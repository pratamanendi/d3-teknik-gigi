<?php
require_once 'mysqlConnection.php';
?>


<!DOCTYPE html>
<html class="scroll-smooth">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link
        href="https://fonts.googleapis.com/css2?family=Abhaya+Libre:wght@400;500;700&family=Montserrat:wght@100;200;300;400;500;600;700;800;900&display=swap"
        rel="stylesheet">
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-79249620-1"></script>
    <link rel="stylesheet" href="./assets/css/style.css">

    <script type="text/javascript">
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());
        gtag('config', 'UA-79249620-1');
    </script>

    <!-- Facebook Pixel Code -->
    <script>
        ! function (f, b, e, v, n, t, s) {
            if (f.fbq) return;
            n = f.fbq = function () {
                n.callMethod ?
                    n.callMethod.apply(n, arguments) : n.queue.push(arguments)
            };
            if (!f._fbq) f._fbq = n;
            n.push = n;
            n.loaded = !0;
            n.version = '2.0';
            n.queue = [];
            t = b.createElement(e);
            t.async = !0;
            t.src = v;
            s = b.getElementsByTagName(e)[0];
            s.parentNode.insertBefore(t, s)
        }(window, document, 'script',
            'https://connect.facebook.net/en_US/fbevents.js');
        fbq('init', '761598857328139');
        fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
            src="https://www.facebook.com/tr?id=761598857328139&ev=PageView&noscript=1" /></noscript>
    <title>D3 Teknik Gigi</title>
</head>

<body class="font-body overflow-x-hidden">
    <div class="relative hidden">
        <div
            class="fixed left-1/2 -translate-x-1/2 my-2 h-8 w-32 rounded-full bg-teal-400 2xs:bg-red-500 xs:bg-red-900 sm:bg-yellow-500 md:bg-green-500 lg:bg-blue-500 xl:bg-indigo-500 2xl:bg-black z-10 font-semibold text-white text-xl grid place-items-center">
            <em>Nendi</em>
        </div>
    </div>
    <section>
        <div
            class="w-screen lg:h-screen bg-white bg-contain bg-right-bottom bg-no-repeat md:bg-[url('../img/asset-86.webp')] md:overflow-hidden">
            <div class="md:flex relative">
                <div class="md:flex-auto md:w-1/2 items-center">
                    <div class="content-top md:relative md:flex md:flex-col md:h-screen md:justify-center">
                        <div
                            class="logo-wrapper text-center md:text-left md:absolute md:top-0 md:w-64 lg:w-auto py-10 px-12">
                            <a href="https://iik.ac.id">
                                <img class="inline" src="./assets/img/logo.webp" />
                            </a>
                        </div>
                        <div class="w-full md:w-1/2 lg:w-2/3 mb-5 px-10 md:mb-10 md:px-16 text-blue-900">
                            <h2
                                class="text-3xl md:text-2xl 2xs:text-xl text-left font-bold leading-none 2xs:px-8 mb-10">
                                Ada banyak perguruan tinggi yang membuka program studi s1 Kedokteran Gigi. Namun, masih
                                sedikit perguruan tinggi yang memiliki prodi D3 Teknik Gigi sekaligus. IIK Bhakta
                                menjadi salah satu perguruan tinggi yang menyediakan dua prodi tersebut
                            </h2>

                            <p class="font-bold text-xl 2xs:px-8">
                                Mahir dalam berteknik gigi tiruan dan menjadi partner terbaik dokter gigi.
                            </p>

                            <p class="2xs:px-8">
                                Profesi ini tidak langsung berhubungan dengan pasien. Tekniker gigi berperan dalam
                                membuat gigi tiruan yang telah di design sebelumnya oleh dokter gigi. sebagai mitra,
                                dokter gigi dan teknisi gigi bekerjasama untuk meningkatkan taraf kesehatan gigi dan
                                mulut masyarakat.
                            </p>

                        </div>
                        <div
                            class="reward-wrapper justify-center md:justify-start items-center  md:absolute md:bottom-0 py-5 lg:py-10 px-16 hidden md:flex">
                            <div class="w-56 lg:w-64">
                                <img class="mx-auto" src="./assets/img/rw-1.png" />
                            </div>
                            <div class="w-32 px-3">
                                <img class="mx-auto" src="./assets/img/rw-2.png" />
                            </div>
                            <div class="w-32 px-3">
                                <img class="mx-auto " src="./assets/img/rw-3.png" />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="md:flex-auto md:w-1/2 md:hidden">
                    <img class="w-auto" src="./assets/img/asset-86.webp" />
                    <div
                        class="reward-wrapper flex justify-center md:justify-start items-center  md:absolute md:bottom-0 py-5 lg:py-10 px-16">
                        <div class="w-56 lg:w-64">
                            <img class="mx-auto" src="./assets/img/rw-1.png" />
                        </div>
                        <div class="w-32 px-3">
                            <img class="mx-auto" src="./assets/img/rw-2.png" />
                        </div>
                        <div class="w-32 px-3">
                            <img class="mx-auto " src="./assets/img/rw-3.png" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div
            class="w-screen lg:h-screen bg-white bg-contain bg-right-bottom bg-no-repeat md:bg-[url('../img/asset-87.webp')] md:overflow-hidden">
            <div class="md:flex relative">
                <div class="md:flex-auto md:w-1/2 items-center">
                    <div class="content-top md:relative md:flex md:flex-col md:h-screen md:justify-center">
                        <div class="w-full md:w-1/2 lg:w-2/3 mb-5 px-10 md:mb-10 md:px-16 text-blue-900 lg:space-y-6">
                            <h2
                                class="text-teal-600 text-3xl md:text-2xl lg:text-3xl xl:text-4xl 2xs:text-xl text-left font-bold leading-none 2xs:px-8 mb-10">
                                Tingginya Kebutuhan
                                <br class="lg:block">
                                Tekniker Gigi di Indonesia
                            </h2>

                            <p class="text-teal-600 font-bold text-lg 2xs:px-8">
                                Sudah Siapkah Anda Jemput
                                <br class="lg:block">
                                Peluang Emas ini Bersama IIK Bhakta?
                            </p>

                            <p class="2xs:px-8 lg:w-5/6 text-sm">
                                Tidak ada satu orang pun yang tidak membutuhkan gigi dalam
                                hidupnya. Anda dapat bayangkan ada berapa jumlah masyarakat
                                Indonesia dan berapa persen yang akan membutuhkan gigi tiruan
                                untuk mereka?
                            </p>

                            <p class="2xs:px-8 lg:w-5/6 text-sm">
                                Profesi teknisi gigi berkaitan erat dengan dokter gigi, sehingga
                                peluang kerja berbanding lurus dengan kebutuhan dokter gigi.
                                Hanya saja jumlah dokter gigi dan teknisi gigi di Indonesia belum
                                seimbang bahkan bertaut jauh. Menurut data Profil Kesehatan
                                2021 oleh Kemenkes, jumlah dokter gigi di Indonesia mencapai
                                33.652 orang. Namun, jumlah teknisi gigi belum mencapai jumlah
                                tersebut. Dengan demikian peluang profesi teknisi gigi masih
                                banyak dibutuhkan oleh masyarakat Indonesia.
                            </p>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="w-screen">
            <div class="title-wrapper p-10">
                <h3 class="text-3xl text-center font-bold">Testimoni Mahasiswa dan Alumni</h3>
            </div>
            <div class="px-5 md:px-28 md:pb-14 relative">
                <swiper-container class="mySwiper" init="false" pagination="true" pagination-clickable="true"
                    navigation="true" grab-cursor="true" space-between="10" slide-per-view="1">
                    <swiper-slide>
                        <img src="./assets/img/slide-1.webp" class="w-full">
                    </swiper-slide>
                    <swiper-slide>
                        <img src="./assets/img/slide-2.webp" class="w-full">
                    </swiper-slide>
                    <swiper-slide>
                        <img src="./assets/img/slide-3.webp" class="w-full">
                    </swiper-slide>
                </swiper-container>
                <div
                    class="swiper-button-next lg:w-12 w-8 after:content-[''] absolute top-1/2 lg:right-10 hidden md:block">
                    <img src="./assets/img/next.png" />
                </div>
                <div class="swiper-button-prev lg:w-12 w-8 after:content-[''] absolute top-1/2 lg:left-10 hidden md:block">
                    <img src="./assets/img/prev.png" />
                </div>
            </div>
        </div>
    </section>
    <!-- not 1st section -->
    <section>
        <!-- mb-5 px-10 md:mb-10 md:px-16 -->
        <div class="w-full flex bg-stone-100 justify-between lg:pl-16 px-4">
            <div class="lg:w-7/12 w-full space-y-5 text-justify p-12 ">
                <div class="space-y-5">
                    <h3 class="font-bold text-2xl">Apa Kata Kaprodi tentang Prodi D3 Teknik Gigi?</h3>
                    <p>"Selama ini, lulusan teknik gigi tidak memerlukan waktu tunggu yang lama untuk mendapatkan
                        pekerjaan, hal ini terlihat dari prosentase keterserapan lulusan yang selalu kami monitoring."
                    </p>
                </div>
                <div class="">
                    <h5 class="font-bold text-lg">Mengapa harus memilih prodi ini?</h5>
                    <p class="lg:mb-12">Prodi D3 Teknik Gigi adalah program studi keahlian dibidang keteknisian gigi
                        yang tidak banyak di Indonesia. Sebagai mitra dokter gigi dalam bekerja, lulusan prodi D3
                        Teknik Gigi sangat menjanjikan dalam hal peluang kerja dan keterserapan lulusan.
                        Lulusan program studi teknik gigi dapat langsung bekerja dengan mendirikan
                        Laboratorium mandiri, Laboratorium mitra atau bekerja di instansi pemerintah seperti
                        di Rumah Sakit Peluang kerja untuk lulusan prodi D3 Teknik Gigi sangat menjanikan, hal ini
                        dapat dilihat dari Rasio perguruan tinggi Dokter Gigi yang sangat banyak di Indonesia,
                        sehingga kebutuhan akan tenaga teknik gigi juga akan sebanding dengan
                        banyaknya Dokter Gigi.</p>
                </div>
                <!-- ================== masa tunggu infobox ================== -->
                <div class="space-y-2">
                    <h5 class="font-bold text-lg">Masa tunggu kerja lulusan IIK Bhakta</h5>
                    <div class="md:w-3/5 sm:w-3/4 mb-96 border-2 lg:w-1/2 border-teal-600 rounded-lg">
                        <div class="flex divide-x-2 items-start divide-teal-600 p-4 ">
                            <div class="px-4 justify-self-center text-left w-1/2">
                                <h5 class="text-5xl text-sky-600 font-bold">99%</h5>
                                <p class="font-semibold text-sm">Memperoleh Kerja dalam waktu kurang dari 6 bulan
                                    setelah lulus
                                </p>
                            </div>
                            <div class="px-4 justify-self-center text-left w-1/2">
                                <h5 class="text-5xl text-sky-600 font-bold">97%</h5>
                                <p class="font-semibold text-sm">Bekerja sesuai bidang
                                </p>
                                <p class="text-thin font-medium text-xs text-slate-500">* Data Tracer Study lulusan
                                    2017-2020</p>
                            </div>
                        </div>
                        <p class="bg-teal-600 font-semibold text-white text-left py-4 px-6">Siap menerima Anda dari
                            segala
                            disiplin IPA,
                            IPS, Bahasa, dll.</p>
                    </div>
                </div>
                <!-- ================== masa tunggu infobox ================== -->

            </div>
            <div class="relative self-end lg:w-5/12 w-0">
                <img src="./assets/img/asset-91.webp" alt="" class="w-full" srcset="">
            </div>
        </div>
    </section>
    <section>
        <div class="w-full">
            <div class="title-wrapper py-10 px-2 md:px-28">
                <h3 class="text-3xl text-center font-bold">Kampus Inovatif dan Modern</h3>
            </div>
            <div class="content-wrapper px-5 md:px-24 pb-28">
                <div class="flex flex-col  sm:flex-row">
                    <div class="sm:flex-1 w-full sm:w-1/3">
                        <div class="content-background mb-5 sm:mb-0 relative sm:px-5">
                            <img class="w-full" src="./assets/img/asset-12.webp" />
                            <div class="z-10 absolute bottom-5 left-0 w-full">
                                <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">Support
                                    Mahasiswa untuk Selalu Berkarya dan Berprestasi</p>
                            </div>
                        </div>
                    </div>
                    <div class="sm:flex-1 w-full mb-5 sm:mb-0 sm:w-1/3">
                        <div class="content-background relative sm:px-5">
                            <img class="w-full" src="./assets/img/asset-13.webp" />
                            <div class="z-10 absolute bottom-5 left-0 w-full">
                                <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">
                                    Aktif Jalin Kerjasama dan Siap Menjadi Garda Terdepan dalam Kesehatan
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="sm:flex-1 w-full mb-5 sm:mb-0 sm:w-1/3">
                        <div class="content-background relative sm:px-5">
                            <img class="w-full" src="./assets/img/asset-14.webp" />
                            <div class="z-10 absolute bottom-5 left-0 w-full">
                                <p class="text-white font-bold w-3/4 lg:w-2/3 text-center mx-auto text-lg">
                                    Selain Kuliah, Mahasiswa IIK Bhakta juga aktif dalam sosial media dengan konten
                                    inspiratif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-full bg-zinc-300 bg-cover bg-center bg-no-repeat bg-[url('../img/bg-section-3.webp')]">
            <div class="content-wrapper px-5 sm:px-24">
                <div class="md:flex md:flex-row">
                    <div class=" hidden lg:block pt-16 flex-auto w-2/6 pb-0">
                        <div class="img-wrapper ">
                            <img class="w-full " src="./assets/img/asset-15.webp" />
                        </div>
                    </div>
                    <div class="md:flex-auto md:w-4/6 md:pt-20 2xs:py-20 xl:pb-0 md-py-0">
                        <!-- <div class="content text-white text-[24px] sm:text-2xl  md:text-3xl text-center md:text-left relative md:top-[50%] md:transform md:translate-y-[-50%] md:pl-16"> -->
                        <div
                            class="content text-white text-[24px] sm:text-2xl  md:text-3xl text-center md:text-left relative md:top-[50%] md:transform md:translate-y-[-50%] md:pl-16">
                            <p class="mb-5"><strong>Tunggu Apalagi ?</strong></p>
                            <p class="mb-5">Bergabunglah sekarang menjadi bagian<br class=" hidden sm:block" /> dari
                                Keluarga Besar IIK Bhakti Wiyata untuk</p>
                            <p class="mb-5 italic"><strong>"Maju membangun dunia yang lebih sehat!"</strong></p>
                            <a href="https://iik.ac.id/pendaftaran"
                                class="bg-teal-600 rounded-full px-3 sm:px-8 py-3 text-sm sm:text-lg"
                                target="_blank">Form Pendaftaran Online</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div
            class="w-full bg-teal-600 overflow-hidden relative before:bg-contain before:absolute before:left-2 lg:before:left-16 before:bottom-0 lg:before:bottom-[-50px] before:opacity-30 lg:before:opacity-100 before:w-40 lg:before:w-64 before:h-32 lg:before:h-48 before:content-[''] before:bg-no-repeat before:bg-center before:bg-[url('../img/bg-section-4.png')] after:bg-contain after:absolute after:right-2 lg:after:right-16 after:top-10 lg:after:top-0 lg:after:w-64 lg:after:h-48 after:w-40 after:h-32 after:content-[''] after:bg-no-repeat after:bg-top after:bg-[url('../img/bg-section-5.png')] after:opacity-30 lg:after:opacity-100">
            <div class="content-wrapper px-5 py-8 sm:px-24">
                <h3 class="font-display font-bold italic text-xl sm:text-3xl lg:text-5xl text-center text-white">Semakin
                    cepat daftarnya, semakin<br />BESAR Diskonnya!</h3>
            </div>
        </div>
        <div
            class="w-screen bg-white bg-cover bg-right-bottom bg-no-repeat bg-[url('../img/asset-6.png')] md:overflow-hidden">
            <div class="content-wrapper py-10 md:pb-0 lg:py-0 px-5 sm:px-8 lg:px-20">
                <div class="flex flex-row">
                    <div class="md:block flex-auto w-3/5 2xl:w-3/6">
                        <div class="flex h-full flex-col justify-center">
                            <div class="content text-sm sm:text-base md:text-sm lg:text-base pb-24 pt-16">
                                <h4 class="mb-5 font-bold">Biaya apa saja di IIK Bhakta?</h4>
                                <p>Biaya Kuliah di IIK Bhakta meliputi Sumbangan Pendidikan, DP3, SPP per semester,
                                    Biaya Pra Studi, Perlengkapan Praktikum, Biaya PKL, PBL dan Wisuda.</p>
                                <h4 class="my-5 font-bold">Ketentuan pembayaran DP3</h4>
                                <p>Biaya DP3 adalah Dana Partisipasi Penyelenggaraan Pendidikan yang memiliki dua metode
                                    pembayaran yaitu langsung dibayar lunas atau dicicil selama 10X untuk semua prodi
                                    dan 20x khusus bagi S1 Kedokteran Gigi. Biaya DP3 jika dibayar lunas saat DU, maka
                                    akan mendapat diskon langsung sebsar 10% dari jumlah D3 normal, tetapi bila dicicil
                                    maka IIK Bhakta memberi kemudahan dibayar awal saat DU dan dilanjutkan cicilan
                                    hingga 10x atau 20x per bulannya</p>
                                <h4 class="my-5 font-bold">Informasi RPL</h4>
                                <p>Jalur RPL (Rekognisi Pembelajaran Lampau) adalah program penyetaraan akademik dari
                                    pendidikan yang telah ditempuh sebelumnya dan telah dibuktikan dengan ijazah untuk
                                    memperoleh kualifikasi pendidikan lebih tinggi pada program studi IIK Bhakta
                                    tertentu. Ada 6 program studi yang ditawarkan program RPL di IIK Bhakta,
                                    diantaranya:</p>
                                <ol class="list-decimal pl-5 mb-5">
                                    <li>D4 Teknologi Laboratorium Medis</li>
                                    <li>S1 Kesehatan Masyarakat</li>
                                    <li>S1 Keperawatan</li>
                                    <li>S1 Administrasi Rumah Sakit</li>
                                    <li>D4 Pengobatan Tradisional Tiongkok</li>
                                    <li>S1 Farmasi</li>
                                </ol>
                                <p>Info lebih detail mengenail RPL dapat langsung cek di halaman <a
                                        href="www.iik.ac.id/RPLProgsus">www.iik.ac.id/RPLProgsus</a></p>
                            </div>
                        </div>

                    </div>
                    <div class="hidden md:block flex-auto w-3/6">
                        <div class="img-wrapper md:h-full md:flex md:flex-col md:items-end md:justify-end">
                            <img class="w-full" src="./assets/img/asset-16.webp" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-screen px-10 lg:px-20 py-16 bg-slate-300 md:overflow-hidden">
            <div class="sm:border-2 sm:border-white">
                <div class="hidden sm:grid grid-cols-1 sm:grid-cols-4 content-center">
                    <div
                        class="text-white text-sm lg:text-base bg-teal-600 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">
                        Jenis Biaya</div>
                    <div
                        class="text-white text-sm lg:text-base bg-teal-600 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">
                        Jareg 1<br />(03 Oktober - 04 Februari 2023)</div>
                    <div
                        class="text-white text-sm lg:text-base bg-teal-600 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">
                        Jareg 2<br />(06 Februari - 03 Juni 2023)</div>
                    <div
                        class="text-white text-sm lg:text-base bg-teal-600 px-5 lg:px-10 py-5 text-center font-bold border border-white flex items-center flex-col justify-center">
                        Jareg 3<br />(05 Juni - 26 Agustus 2023)</div>
                </div>
                <div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
                    <div
                        class="text-white sm:text-black sm:bg-teal-100 2xs:bg-teal-600 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex flex-col items-center sm:items-start justify-center">
                        DP3 Normal
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 1<br />(03 Oktober - 04 Februari 2023)</span>
                        <br />Rp 5.444.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 2<br />(06 Februari - 03 Juni 2023)</span>
                        <br />Rp 5.556.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 3<br />(05 Juni - 26 Agustus 2023)</span>
                        <br />Rp 5.667.000
                    </div>
                </div>
                <div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
                    <div
                        class="text-white sm:text-black sm:bg-teal-100 2xs:bg-teal-600 text-sm md:text-base px-5 lg:px-10 py-5 text-left font-bold border border-white flex flex-col items-center sm:items-start justify-center">
                        DP3 (Angsuran Awal Saat DU)
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 1<br />(03 Oktober - 04 Februari 2023)</span>
                        <br />Rp 2.994.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 2<br />(06 Februari - 03 Juni 2023)</span>
                        <br />Rp 3.056.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 3<br />(05 Juni - 26 Agustus 2023)</span>
                        <br />Rp 3.117.000
                    </div>
                </div>
                <div class="grid grid-cols-1 sm:grid-cols-4 mb-10 sm:mb-0 content-center">
                    <div
                        class="text-white sm:text-black sm:bg-teal-100 2xs:bg-teal-600 text-sm md:text-base px-5 lg:px-10 py-5 font-bold border border-white flex flex-col items-center sm:items-start text-center sm:text-left justify-center">
                        DP3 Angsuran DP3 per Bulan (Selama 10x)
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 1<br />(03 Oktober - 04 Februari 2023)</span>
                        <br />Rp 245.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 2<br />(06 Februari - 03 Juni 2023)</span>
                        <br />Rp 250.000
                    </div>
                    <div
                        class="bg-teal-100 text-sm md:text-base px-5 lg:px-10 py-5 text-center border border-white flex items-center flex-col justify-center">
                        <span class="block sm:hidden font-bold mb-3">Jareg 3<br />(05 Juni - 26 Agustus 2023)</span>
                        <br />Rp 255.000
                    </div>
                </div>
            </div>
        </div>
        <div class="w-screen px-5 sm:px-10 lg:px-20 py-16 bg-white md:overflow-hidden">
            <div class="grid grid-cols-1 sm:grid-cols-2 md:grid-cols-3  content-center">
                <div class="left-content">
                    <div class="btn-wrapper text-center sm:border-r-2 sm:border-black">
                        <a href="https://iik.ac.id/web.php?judul=kuis_penentu_prodi_iik_bhaktiwiyata&r=kuis_penentu_prodi"
                            target="_blank"
                            class="block mx-auto text-sm lg:text-base mb-5 border border-black font-bold px-10 lg:px-5 py-4 rounded-full sm:max-w-[85%]">Kuis
                            Penentuan Prodi Anda</a>
                        <a href="https://drive.google.com/drive/u/0/folders/1Hv3HNhCpvvQbq8_3C5AX6BRZ0nQuT2Gd"
                            class="block mx-auto text-sm lg:text-base border border-black font-bold px-2 md:px-3 py-4 rounded-full  sm:max-w-[85%]">Download
                            Brosur dan Welcome Book Prodi</a>
                    </div>
                </div>
                <div class="center-content mt-5 sm:mt-0">
                    <div class="btn-wrapper md:border-r-2 md:border-black text-center pb-[15px]">
                        <h4 class="font-bold text-sm md:text-base">Admin Kami Siap Menjawab Pertanyaan Anda</h4>
                        <a href="https://api.whatsapp.com/send/?phone=6285717171735&text&app_absent=0"
                            class="block mx-auto border border-black px-5 lg:px-2 py-4 rounded-full mt-5 mb-5 sm:max-w-[90%] md:max-w-[85%]">
                            <img class="inline-block md:hidden lg:inline-block align-bottom"
                                src="./assets/img/asset-17.png" /> <span class="inline-block"> Chat WA Admin
                                Pendaftaran</span></a>
                    </div>
                </div>
                <div class="right-content sm:col-span-2 md:col-span-1">
                    <div class="content-wrapper md:pl-10 text-center md:text-left">
                        <h4 class="font-bold inline-block md:block ">Link :</h4>
                        <ul class="py-4 inline-block md:block pl-3 md:pl-0">
                            <li class="text-lg inline-block mr-3 md:block md:mr-0">
                                <a href="https://iik.ac.id/web.php?judul=info_beasiswa_iik_bhaktiwiyata&r=pendaftaran&sp=infobeasiswa"
                                    target="_blank" class="text-red-800"> Info Beasiswa</a>
                            </li>
                            <li class="text-lg inline-block mr-3 md:block md:mr-0">
                                <a href="https://iik.ac.id/web.php?judul=tata_cara_daftar_iik_bhaktiwiyata&r=pendaftaran&sp=tatacaradaftar"
                                    target="_blank" class="text-red-800"> Tata Cara Pendaftaran</a>
                            </li>
                            <li class="text-lg inline-block mr-3 md:block md:mr-0">
                                <a href="https://iik.ac.id/web.php?judul=info_fasilitas_dan_peta_kampus_iik_bhaktiwiyata&r=pendaftaran&sp=turkampus"
                                    target="_blank" class="text-red-800"> Virtual Tour Campus</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="w-screen md:min-h-screen px-10 lg:px-20 py-16 bg-slate-300 md:overflow-hidden">
            <div class="md:flex justify-center items-center gap-4">
                <div class="md:w-1/2 w-full">
                    <div class="sm:text-sm lg:text-base pb-10 text-lg">
                        <h3 class="font-bold text-lg sm:text-2xl mb-5">Sambutan Rektor</h3>
                        <p class="mb-5 text-justify">Selamat datang di Institut Ilmu Kesehatan Bhakti Wiyata
                            Kediri, institut kesehatan pertama di Indonesia. Kami sangat komit dengan harapan
                            kami agar pendidikan tinggi kesehatan berkualitas dapat menjangkau berbagai kalangan
                            dari segala penjuru Indonesia. Dengan demikian, harapannya tenaga kesehatan
                            profesional berasal dari berbagai kalangan dan daerah, sehingga nanti kemajuan
                            kesehatan Indonesia juga dapat merata di negeri ini. </p>
                        <p class="mb-5 text-justify">Kami akan berusaha semaksimal mungkin menjadikan anak didik
                            tenaga kesehatan yang andal dan profesional dengan memberikan pengetahuan kesehatan
                            terkini, keterampilan dan kematangan pribadi yang kelak akan menjadi bekal dalam
                            menjalankan profesinya di tengah masyarakat. Percayakan pendidikan berkualitas
                            bersama kami IIK Bhakta!</p>
                    </div>
                </div>
                <div class="md:w-1/2 w-full relative overflow-hidden pt-[56.25%] border-2 border-slate-300">
                    <iframe class="yt-frame" src="https://www.youtube.com/embed/7ktp65pjrk4"
                        title="YouTube video player" frameborder="0"
                        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                        allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="w-screen px-10 lg:px-20">
			<div class="content-wrapper py-14">
				<div class="title text-center">
					<h3 class="text-2xl lg:text-4xl font-bold">Partner Kerjasama Nasional</h3>
				</div>
				<div class="img-wrapper text-center py-10">
					<div class="grid grid-cols-3 sm:grid-cols-5 lg:grid-cols-8 gap-2 sm:justify-items-center">
						<?php for ($i = 1; $i <= 27; $i++) { ?>
							<div class="partner-wrapper px-2 py-4">
								<img class="max-w-full" src="./assets/partner-nasional/partner-<?= $i; ?>.png" />
							</div>
						<?php } ?>
					</div>
				</div>
				<div class="title text-center">
					<h3 class="text-2xl lg:text-4xl font-bold">Partner Kerjasama Internasional</h3>
				</div>
				<div class="img-wrapper text-center py-10">
					<div class="grid grid-cols-3 sm:grid-cols-4 lg:grid-cols-6 gap-2 justify-items-center">
						<?php for ($i = 1; $i <= 12; $i++) { ?>
							<div class="partner-wrapper px-2 py-4">
								<img class="max-w-full" src="./assets/partner-internasional/partner-<?= $i; ?>.png" />
							</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
    </section>
    <footer>
        <div
            class="w-screen px-5 sm:px-20 py-20 bg-blue-900 bg-cover bg-center bg-no-repeat bg-[url('../img/footer-bg.webp')]">
            <div class="grid grid-cols-6 gap-4">
                <div class="col-span-6 mb-4 md:mb-0 lg:col-span-2">
                    <img class="w-[75%]" src="./assets/img/logo_white.png" />
                </div>
                <div class="footer-content col-span-6 mb-4 md:mb-0 md:col-span-2 lg:col-span-2">
                    <h4 class="text-white font-bold text-xl">Site Traffic</h4>
                    <div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-3">
                        <div class="flex-auto">
                            <img class="w-auto" src="./assets/img/24hrs_white.png" />
                        </div>
                        <div class="flex-auto">
                            <span class="text-sm text-white">Last 24 hrs</span>
                        </div>
                        <div class="flex-none">
                            <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">
                                <?= number_format($onFirst); ?>
                            </div>
                            <!-- <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">200</div> -->
                        </div>
                    </div>
                    <div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
                        <div class="flex-auto">
                            <img class="w-auto" src="./assets/img/7days_white.png" />
                        </div>
                        <div class="flex-auto">
                            <span class="text-sm text-white">Last 7 days</span>
                        </div>
                        <div class="flex-none">
                            <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">
                                <?= number_format($onSeven); ?>
                            </div>
                            <!-- <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">2090</div> -->
                        </div>
                    </div>
                    <div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
                        <div class="flex-auto">
                            <img class="w-auto" src="./assets/img/30days_white.png" />
                        </div>
                        <div class="flex-auto">
                            <span class="text-sm text-white">Last 30 days</span>
                        </div>
                        <div class="flex-none">
                            <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">
                                <?= number_format($onthree); ?>
                            </div>
                            <!-- <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">300</div> -->
                        </div>
                    </div>
                    <div class=" w-full sm:w-2/5 md:w-auto flex justify-start mt-2">
                        <div class="flex-auto">
                            <span class="text-base text-white font-bold">Online Now</span>
                        </div>
                        <div class="flex-none">
                            <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">
                                <?= number_format($online); ?>
                            </div>
                            <!-- <div class="w-24 mr-4 h-5 bg-white text-right text-blue-800 px-2">1000</div> -->
                        </div>
                    </div>
                </div>
                <div class="footer-content col-span-6 md:col-span-4 lg:col-span-2">
                    <h4 class="text-white font-bold text-lg sm:text-xl">Institut Ilmu Kesehatan Bhakti Wiyata</h4>
                    <div class="grid grid-cols-3 gap-2">
                        <div class="contact-wrapper col-span-3 sm:col-span-2 mt-3">
                            <div class="flex items-center">
                                <div class="flex-none pr-2">
                                    <img class="w-auto" src="./assets/img/marker_white.png" />
                                </div>
                                <div class="flex-auto pl-2 border-l-2 border-white">
                                    <span class="text-base text-white">Jl. KH. Wahid Hasyim 65<br />Kediri 64114 Jawa
                                        Timur</span>
                                </div>
                            </div>
                            <div class="flex items-center mt-2">
                                <div class="flex-none pr-2">
                                    <img class="w-3" src="./assets/img/phone_white.png" />
                                </div>
                                <div class="flex-auto pl-2 border-l-2 border-white">
                                    <span class="text-base text-white">Tlp. <a href="tel:0354773299">0354 773299</a> /
                                        <a href="tel:0354773535">0354 773535</a></span>
                                </div>
                            </div>
                            <div class="flex items-center mt-2">
                                <div class="flex-none pr-2">
                                    <img class="w-3" src="./assets/img/fax_white.png" />
                                </div>
                                <div class="flex-auto pl-2 border-l-2 border-white">
                                    <a class="text-base text-white" href="fax:0354721539">Fax. 0354 721539</a>
                                </div>
                            </div>
                            <div class="flex items-center mt-2">
                                <div class="flex-none pr-2">
                                    <img class="w-3" src="./assets/img/wa_white.png" />
                                </div>
                                <div class="flex-auto pl-2 border-l-2 border-white">
                                    <a class="text-base text-white" href="https://wa.me/6285717171735">Whatsapp 0857
                                        1717 1735</a>
                                </div>
                            </div>
                        </div>
                        <div
                            class="map-wrapper h-full flex flex-col col-span-3 sm:col-span-1 justify-center items-center pt-2">
                            <iframe
                                src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d538.8116192820244!2d112.00567632095293!3d-7.81738692213297!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xafc394ffd8990c4d!2sInstitut%20Ilmu%20Kesehatan%20Bhakti%20Wiyata!5e0!3m2!1sid!2sid!4v1640838293695!5m2!1sid!2sid"
                                width="100%" height="150" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-blue-900 px-20 py-5">
            <div class="grid grid-cols-2">
                <div class="sosmed-wrapper col-span-2 sm:col-span-1">
                    <div class="flex justify-center sm:justify-start">
                        <a href="http://www.instagram.com/iikbhaktiwiyata/" class="mr-8">
                            <img class="w-auto" src="./assets/img/ig_white.png" />
                        </a>
                        <a href="http://www.facebook.com/iikbwkediri" class="mr-8">
                            <img class="w-auto" src="./assets/img/fb_white.png" />
                        </a>
                        <a href="http://www.youtube.com/channel/UCWWpX0nV9b4fNPQt5dNWy5w" class="mr-8">
                            <img class="w-auto" src="./assets/img/yt_white.png" />
                        </a>
                        <a href="https://iik.ac.id/#" class="mr-8">
                            <img class="w-auto" src="./assets/img/twitter_white.png" />
                        </a>
                    </div>
                </div>
                <div class="copy-wrapper text-white text-center col-span-2 mt-4 sm:mt-0 sm:col-span-1">
                    <span>&copy;</span> <a href="https://iik.ac.id/">IIK Bhakti Wiyata</a>
                </div>
            </div>
        </div>
    </footer>
    <div class="fix-wrapper z-10 fixed right-0 bottom-0 p-5">
        <a href="https://api.whatsapp.com/send/?phone=6285717171735&text&app_absent=0" class="block">
            <img class="w-40 sm:w-64" src="./assets/img/wa-btn.png" />
        </a>
        <a href="https://iik.ac.id/devweb2/home/indexin.php?opt=olreg" class="mt-2 block">
            <img class="w-40 sm:w-64" src="./assets/img/daftar-btn.png" />
        </a>
    </div>

</body>
<script src="./assets/swiper/swiper-element-bundle.min.js"></script>
<script>
    const swiperEl = document.querySelector('.mySwiper')
    const bp = {
        "@0.00": {
            slidesPerView: 1,
        },
        "@0.75": {
            slidesPerView: 2,
        },
        "@1.00": {
            slidesPerView: 3,
        }
    }

    Object.assign(swiperEl, {
        breakpoints: bp,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }
    });
    swiperEl.initialize();
</script>

</html>